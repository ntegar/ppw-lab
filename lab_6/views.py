# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def index(request):
    response = {'author': "Nabil Tegar"}
    return render(request, 'lab_6/lab_6.html', response)
