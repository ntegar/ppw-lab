from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

import os
import json

# Create your views here.
def index(request):
    response = {}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)