from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'I am currently an undergraduate at Fasilkom UI majoring in Information Systems and I am proud to be a part of OMEGA 2016. I love to have friends, and maybe you could be my next friend too ;)'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)